# README #

# Sodelujoči #
* Benjamin Medvešček Murovec

#Povzetek#
* Ta igra je nedokončana, in ima samo najosnovnejše stvari implementirane.
* Igra kjer upravljaš kvadratno vesoljsko ladjo in streljaš nasprotnike.

# Opis #
* Ladja vedno gleda v smeri miške.
* Premikanje z tipkami (W, A, S, D).

#To Do#
* Bug: napaka nekje pri rotaciji kvadrata za miško.
* Ustvari nasprotnike (Script, prefab,...)
* Naredi "combat" (Script za streljanje, prefab,...)