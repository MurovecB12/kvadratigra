﻿using UnityEngine;
using System.Collections;

public class rotacija_trikotnika : MonoBehaviour {

	private Vector2 prvaPozicijaMiske = new Vector2(0,0);
	private Vector2 drugaPozicijaMiske = new Vector2(0,0);
	private float kotRotacije;

	// Use this for initialization
	void Start () {
		prvaPozicijaMiske = Input.mousePosition;
	}
	
	// Update is called once per frame
	void Update () {
		drugaPozicijaMiske = Input.mousePosition;
		Vector2 pozicijaKvadrata = this.transform.position;

		kotRotacije = izracunajKotRotacije(prvaPozicijaMiske,drugaPozicijaMiske,pozicijaKvadrata);
		this.transform.Rotate (new Vector3 (0, 0, kotRotacije));
		Debug.Log (kotRotacije);
		prvaPozicijaMiske = drugaPozicijaMiske;

		if (Input.GetKeyDown(KeyCode.Q)) {
			zavrti (4,1);		
		}
		if (Input.GetKeyDown(KeyCode.E)) {
			zavrti (4,-1);		
		}

	}


	void zavrti(int n,int smer){
		this.transform.Rotate(new Vector3(0, 0, smer*360/n));
	}

	float izracunajKotRotacije(Vector2 prvi, Vector2 drugi,Vector2 center){
		Vector2 pr = prvi-center;
		Vector2 dr = drugi-center;
		return (Mathf.PI/180)*Mathf.Acos (Vector2.Dot (pr,dr)/(pr.magnitude * dr.magnitude));
	}


}
